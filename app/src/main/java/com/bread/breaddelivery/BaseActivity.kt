package com.bread.breaddelivery

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bread.breaddelivery.utils.DialogUtil


open class BaseActivity : AppCompatActivity() {
    private var dlgProgress: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected fun showProgressDialog() {
        dlgProgress?.dismiss()
        dlgProgress = this?.let { DialogUtil.showProgressDialog(it) }
    }

    fun hideProgressDialog() {
        dlgProgress?.dismiss()
    }
}