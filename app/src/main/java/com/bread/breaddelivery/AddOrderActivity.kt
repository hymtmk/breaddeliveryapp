package com.bread.breaddelivery

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.text.format.DateFormat
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.TextFormatter
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import fcm.androidtoandroid.FirebasePush
import fcm.androidtoandroid.model.Notification
import org.json.JSONObject
import java.util.*


class AddOrderActivity : AppCompatActivity() {

    private lateinit var mDate:Date
    private lateinit var edit_request_date: EditText
    private lateinit var edit_small_bread: EditText
    private lateinit var edit_large_bread: EditText
    private lateinit var edit_footlong_bread: EditText
    private lateinit var but_addorder: Button

    private lateinit var mRequestDate: String
    private lateinit var mSmallPcs: String
    private lateinit var mLargePcs: String
    private lateinit var mFootlongPcs: String

    private lateinit var userId:String
    private lateinit var email:String
    private lateinit var customerName: String
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addorder)

        //action bar
        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.text_add_order)
        actionbar.setDisplayHomeAsUpEnabled(true)
        //actionbar.setHomeButtonEnabled(true)
        edit_request_date = findViewById(R.id.edit_request_date)
        edit_request_date.setOnClickListener { view ->
            val calendar = Calendar.getInstance()
            calendar.time = mDate
            val year = calendar[Calendar.YEAR]
            val month = calendar[Calendar.MONTH]
            val day = calendar[Calendar.DAY_OF_MONTH]

            val datePickerDialog =
                DatePickerDialog(this@AddOrderActivity,
                    OnDateSetListener { datePicker, i, i1, i2 -> setStartDate(i, i1, i2) },
                    year,
                    month,
                    day
                )
            datePickerDialog.show()
        }
        edit_small_bread = findViewById(R.id.edit_small_bread)
        edit_large_bread = findViewById(R.id.edit_large_bread)
        edit_footlong_bread = findViewById(R.id.edit_footlong_bread)
        but_addorder = findViewById(R.id.but_addorder)
        but_addorder.setOnClickListener{ view ->
            mRequestDate = edit_request_date?.text.toString()
            mSmallPcs = edit_small_bread?.text.toString()
            mLargePcs = edit_large_bread?.text.toString()
            mFootlongPcs = edit_footlong_bread?.text.toString()
            if (!TextUtils.isEmpty(mSmallPcs) || !TextUtils.isEmpty(mLargePcs)
                || !TextUtils.isEmpty(mFootlongPcs)){
                confirmDialog()
            }else{
                Toast.makeText(this, getString(R.string.text_invalid_userinfo), Toast.LENGTH_SHORT).show()
            }
        }
        userId = intent.getStringExtra(Constant.EXTRA_USERID)!!
        email = intent.getStringExtra(Constant.EXTRA_EMAIL)!!
        customerName = intent.getStringExtra(Constant.EXTRA_CUSTOMERNAME)!!
        setDateAndTimeEditText()

    }
    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    fun confirmDialog(){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(this)
        // Set a title for alert dialog
        builder.setTitle(getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { processConfirm() }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun setStartDate(year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        val time24: Boolean = DateFormat.is24HourFormat(this)
        if (time24) {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1)
        } else {
            calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + 1)
        }
        calendar.set(Calendar.MINUTE, 0)
        val hour: Int
        val minute: Int
        val reminderCalendar = Calendar.getInstance()

        reminderCalendar[year, month] = day

        if(!email.equals("test1@gmail.com")) {
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }
        else{
            if (time24) {
                reminderCalendar.add(Calendar.HOUR_OF_DAY, 1)
            } else {
                reminderCalendar.add(Calendar.HOUR, 1)
            }
        }

        if (reminderCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || reminderCalendar.before(calendar)) { //    Toast.makeText(this, "My time-machine is a bit rusty", Toast.LENGTH_SHORT).show();
            return
        }
        if (mDate != null) {
            calendar.time = mDate
        }
        hour = if (DateFormat.is24HourFormat(this)) {
            calendar[Calendar.HOUR_OF_DAY]
        } else {
            calendar[Calendar.HOUR]
        }
        minute = calendar[Calendar.MINUTE]
        calendar[year, month, day, hour] = minute
        mDate = calendar.time
        val dateFormat = "dd/MM/yyyy"
        edit_request_date.setText(TextFormatter.formatDate(dateFormat, mDate))
    }
    private fun setDateAndTimeEditText() {
        val cal: Calendar = Calendar.getInstance()
        val time24: Boolean = DateFormat.is24HourFormat(this)
        if (time24) {
            cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 1)
        } else {
            cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 1)
        }
        cal.set(Calendar.MINUTE, 0)
        cal.add(Calendar.DAY_OF_MONTH, 1)
        mDate = cal.getTime()
        val dateString: String = TextFormatter.formatDate("dd/MM/yyyy", mDate)!!
        edit_request_date.setText(dateString)
    }
    private fun processConfirm(){
        val key = mDatabaseReference!!.push().key
        val currentOrderDb = mDatabaseReference!!.child(key!!)
        currentOrderDb.child("userid").setValue(userId)
        currentOrderDb.child("date").setValue(mRequestDate)
        currentOrderDb.child("small").setValue(mSmallPcs)
        currentOrderDb.child("large").setValue(mLargePcs)
        currentOrderDb.child("footlong").setValue(mFootlongPcs)
        currentOrderDb.child("status").setValue(0)
        currentOrderDb.child("visible").setValue(true)

        sendPushNotification()
        finish()
    }
    // send push notification
    private fun sendPushNotification(){
        val jsonData= JSONObject()
        jsonData.put("type","add_order")
        jsonData.put("text","Nova Porudžbina od " + customerName + ", klikni za više informacija!")
        FirebasePush.build(getString(R.string.fcm_legacy_server_key))
            .setData(jsonData)
            .sendToTopic(getString(R.string.topic_storage))
}
}
