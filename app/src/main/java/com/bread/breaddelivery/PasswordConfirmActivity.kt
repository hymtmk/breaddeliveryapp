package com.bread.breaddelivery

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.SessionManager
import com.google.firebase.auth.FirebaseAuth

class PasswordConfirmActivity : BaseActivity() {
    private lateinit var but_password_confirm: Button
    private lateinit var mUserName: String
    private lateinit var mEmail: String
    private lateinit var mPassword: String
    private var mLoginMode: Int = 0

    private lateinit var edit_password: EditText
    private lateinit var auth: FirebaseAuth

    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_passwordconfirm)

        val intent = getIntent()
        mLoginMode = intent.getIntExtra(Constant.EXTRA_LOGIN_PARAM, 0)
        when (mLoginMode) {
            0 -> mUserName = getString(R.string.text_admin);
            1 -> mUserName = getString(R.string.text_driver_zone1)
            2 -> mUserName = getString(R.string.text_all_storage)
            else -> { // Note the block
                mUserName = getString(R.string.text_admin);
            }
        }
        edit_password = findViewById(R.id.edit_password)
        var text_username = findViewById<TextView>(R.id.text_username)
        text_username.setText(mUserName)

        but_password_confirm = findViewById<Button>(R.id.but_password_confirm)
        but_password_confirm.setOnClickListener{
            mPassword = edit_password.text.toString()
            if(!mPassword.isEmpty()) {
                when (mLoginMode) {
                    0 -> mEmail = "admin@gmail.com"//mijatovic011
                    1 -> mEmail = "driver@gmail.com"
                    2 -> mEmail = "storage@gmail.com"
                }
                showProgressDialog()
                auth?.signInWithEmailAndPassword(mEmail!!, mPassword!!)
                    .addOnCompleteListener(this) { task ->
                        hideProgressDialog()
                        if (task.isSuccessful) {
                            when (mLoginMode) {
                                0 -> launchMainActivity()
                                1 -> launchDriverOrderActivity()
                                2 -> launchStorageActivity()
                            }
                            // save into preference
                        } else {
                            Toast.makeText(
                                this@PasswordConfirmActivity, getString(R.string.text_auth_failed),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }
        sessionManager = SessionManager(this)
        auth = FirebaseAuth.getInstance()
    }
    // Goto Admin Dashbaord
    fun launchMainActivity(){
        sessionManager.setUserMode(2)
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
    // Goto Driver Order Screen
    fun launchDriverOrderActivity(){
        sessionManager.setUserMode(3)
        var intent = Intent(this, DriverOrderActivity::class.java)
        startActivity(intent)
        finish()
    }
    // Goto Storage Screen
    fun launchStorageActivity(){
        sessionManager.setUserMode(4)
        var intent = Intent(this, StorageActivity::class.java)
        startActivity(intent)
        finish()
    }
}
