package com.bread.breaddelivery

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.get
import com.bread.breaddelivery.utils.Constant
import com.google.firebase.database.*


class EditCustomerActivity : BaseActivity() {

//    private lateinit var spinner_zone: Spinner
    private lateinit var edit_customername: EditText
    private lateinit var edit_address: EditText
    private lateinit var edit_phonenumber: EditText
    private lateinit var edit_distance: EditText
    private lateinit var but_save: Button

    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null

    private lateinit var userId:String
//    private var zone: String? = null
    private var customername: String? = null
    private var address: String? = null
    private var phonenumber: String? = null
    private var distance: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editcustomer)

        //action bar
        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.text_edit_profile)
        actionbar.setDisplayHomeAsUpEnabled(true)
        //actionbar.setHomeButtonEnabled(true)

//        spinner_zone = findViewById<View>(R.id.spinner_zone) as Spinner
        edit_customername = findViewById<View>(R.id.edit_customername) as EditText
        edit_address = findViewById<View>(R.id.edit_address) as EditText
        edit_phonenumber = findViewById<View>(R.id.edit_phonenumber) as EditText
        edit_distance = findViewById<View>(R.id.edit_distance) as EditText

        but_save = findViewById<Button>(R.id.but_save)
        but_save.setOnClickListener{
            saveAccount()
        }
        userId = intent.getStringExtra(Constant.EXTRA_USERID)

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        //update user profile information
        val currentUserDb = mDatabaseReference!!.child(userId)

        showProgressDialog()
        currentUserDb.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
//                zone = snapshot.child("zone").value as String
                customername = snapshot.child("customername").value as String
                address = snapshot.child("address").value as String
                phonenumber = snapshot.child("phonenumber").value as String
                distance = snapshot.child("distance").value as String
                // setText
//                setSpinnerText(zone!!)
                edit_customername.setText(customername)
                edit_address.setText(address)
                edit_phonenumber.setText(phonenumber)
                edit_distance.setText(distance)
                hideProgressDialog()
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }
/*
    private fun setSpinnerText(strValue: String){
        var i:Int = 0;
        while (i < spinner_zone.getCount()) {
            if (strValue.equals(spinner_zone.getItemAtPosition(i) .toString())) {
                spinner_zone.setSelection(i)
                break
            }
            i++
        }
    }
 */
    private fun saveAccount() {
//        zone = spinner_zone.selectedItem.toString()
        customername = edit_customername?.text.toString()
        address = edit_address?.text.toString()
        phonenumber = edit_phonenumber?.text.toString()
        distance = edit_distance?.text.toString()
        if (!TextUtils.isEmpty(customername) && !TextUtils.isEmpty(address) && !TextUtils.isEmpty(phonenumber) && !TextUtils.isEmpty(distance)) {
            //update user profile information
            val currentUserDb = mDatabaseReference!!.child(userId)
//            currentUserDb.child("zone").setValue(zone)
            currentUserDb.child("customername").setValue(customername)
            currentUserDb.child("address").setValue(address)
            currentUserDb.child("phonenumber").setValue(phonenumber)
            currentUserDb.child("distance").setValue(distance)
            finish()
        } else {
            Toast.makeText(this, getString(R.string.text_invalid_userinfo), Toast.LENGTH_SHORT).show()
        }
    }
}
