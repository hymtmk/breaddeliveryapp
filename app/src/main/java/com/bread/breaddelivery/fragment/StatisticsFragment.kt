package com.bread.breaddelivery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Spinner
import androidx.fragment.app.Fragment
import com.bread.breaddelivery.R
import com.bread.breaddelivery.adapter.CustomerListAdapter
import com.bread.breaddelivery.adapter.StatisticsListAdapter
import com.bread.breaddelivery.model.CustomerModel
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.DateUtil
import com.google.firebase.database.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class StatisticsFragment : Fragment() {

    private var spinner_range: Spinner? = null
    private var listView: ListView? = null
    private var adapter: StatisticsListAdapter? = null
    private lateinit var customerOrderList: ArrayList<CustomerOrder>
    private lateinit var statisticsOrderList: ArrayList<CustomerOrder>

    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_statistics, container, false)

        // show list view
        customerOrderList = ArrayList<CustomerOrder>()
        statisticsOrderList = ArrayList<CustomerOrder>()
        spinner_range = root.findViewById<Spinner>(R.id.spinner_range)
        listView = root.findViewById<ListView>(R.id.listView)
        adapter = StatisticsListAdapter(context!!, statisticsOrderList)

        spinner_range?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                getStatisticsOrderList(position)
            }
        }
        listView?.adapter = adapter
        adapter?.notifyDataSetChanged()
        activity!!.setTitle(getString(R.string.menu_statistics))
        return root
    }
    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        var mCustomerOrderRef = mDatabase!!.reference!!.child("CustomerOrders")
        mCustomerOrderRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(orderSnapshot: DataSnapshot) {
                customerOrderList.clear()
                for (childSnapshot in orderSnapshot.children) {
                    var userid = childSnapshot.child("userid").value.toString()
                    var status = childSnapshot.child("status").value.toString()
                    if(status.equals("2")){
                        var requestDate = childSnapshot.child("date").value.toString()
                        var smallVal = childSnapshot.child("small").value.toString()
                        var largeVal = childSnapshot.child("large").value.toString()
                        var footlongVal = childSnapshot.child("footlong").value.toString()
                        customerOrderList.add(CustomerOrder(userid, requestDate, smallVal, largeVal, footlongVal, status))
                    }
                }
                var nPos = spinner_range!!.selectedItemPosition
                getStatisticsOrderList(nPos)
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
    }
    fun getStatisticsOrderList(pos: Int){
        mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        mDatabaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                statisticsOrderList.clear()
                // Get Post object and use the values to update the UI
                for (postSnapshot in dataSnapshot.children) {
                    var uservisible = postSnapshot.child("visible")
                    // in case of visible
//                    if(uservisible.value!!.equals(true)) {
                        var customerKey =  postSnapshot.key.toString()
                        var customerName = postSnapshot.child("customername").value.toString()
                        var address = postSnapshot.child("address").value.toString()
                        var phonenumber = postSnapshot.child("phonenumber").value.toString()
                        var distance = postSnapshot.child("distance").value.toString()
                        var smallPcs :Int = 0
                        var largePcs :Int = 0
                        var footlongPcs :Int = 0
                        for (childSnapshot in customerOrderList) {
                            var userid = childSnapshot.orderid
                            if(userid.equals(customerKey)){
                                var requestDate = childSnapshot.requestDate
                                if(pos == 0) {
                                    if (!DateUtil.isThisDay(requestDate))
                                        continue
                                }
                                else if(pos == 1){
                                    if(!DateUtil.isThisWeek(requestDate))
                                        continue
                                }
                                else if(pos == 2){
                                    if(!DateUtil.isThisMonth(requestDate))
                                        continue
                                }
                                else if(pos == 3) {
                                    if (!DateUtil.isThisYear(requestDate))
                                        continue
                                }
                                var smallVal = childSnapshot.small
                                var largeVal = childSnapshot.large
                                var footlongVal = childSnapshot.footlong
                                if(!smallVal.isEmpty())
                                    smallPcs = smallPcs +  smallVal.toInt()
                                if(!largeVal.isEmpty())
                                    largePcs = largePcs +  largeVal.toInt()
                                if(!footlongVal.isEmpty())
                                    footlongPcs = footlongPcs +  footlongVal.toInt()
                            }
                        }
                        statisticsOrderList.add(CustomerOrder("", "", smallPcs.toString(), largePcs.toString(), footlongPcs.toString(), "", customerName, address, phonenumber, distance))
                        adapter?.notifyDataSetChanged()

//                    }
                    // TODO: handle the post
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
    }

}