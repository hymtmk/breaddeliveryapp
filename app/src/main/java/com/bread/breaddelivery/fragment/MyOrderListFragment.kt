package com.bread.breaddelivery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.bread.breaddelivery.R
import com.bread.breaddelivery.adapter.MyOrderListAdapter
import com.bread.breaddelivery.model.CustomerOrder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class MyOrderListFragment : Fragment() {

    private var listView: ListView? = null
    private var adapter: MyOrderListAdapter? = null
    private lateinit var myOrderList: ArrayList<CustomerOrder>

    private lateinit var auth: FirebaseAuth
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase

    private lateinit var mUserId: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_myorder_list, container, false)

        // show list view
        listView = root.findViewById<ListView>(R.id.listView)
        myOrderList = ArrayList<CustomerOrder>()
        adapter = MyOrderListAdapter(context!!, myOrderList)

        listView?.adapter = adapter
        adapter?.notifyDataSetChanged()
        return root
    }

    override fun onStart() {
        super.onStart()
        auth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        mUserId = auth!!.currentUser!!.uid

        mDatabaseReference.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                myOrderList.clear()
                // Get Post object and use the values to update the UI
                for (postSnapshot in dataSnapshot.children) {
                    var userid = postSnapshot.child("userid")
                    if(userid.value!!.equals(mUserId)){
                        myOrderList.add(CustomerOrder(
                            postSnapshot.key.toString(),
                            postSnapshot.child("date").value.toString(),
                            postSnapshot.child("small").value.toString(),
                            postSnapshot.child("large").value.toString(),
                            postSnapshot.child("footlong").value.toString(),
                            postSnapshot.child("status").value.toString()))
                    }
                    // TODO: handle the post
                }
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                myOrderList.sortWith(compareByDescending<CustomerOrder> { it?.requestDate }
                    .thenByDescending { sdf.parse(it?.requestDate) }
                )
                adapter?.notifyDataSetChanged()
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
        /*
        mDatabaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                for (postSnapshot in dataSnapshot.children) {
                    var userid = postSnapshot.child("userid")
                    if(userid.value!!.equals(mUserId)){
                        myOrderList.add(CustomerOrder(
                            postSnapshot.key.toString(),
                            postSnapshot.child("date").value.toString(),
                            postSnapshot.child("small").value.toString(),
                            postSnapshot.child("large").value.toString(),
                            postSnapshot.child("footlong").value.toString(),
                            postSnapshot.child("status").value.toString()))
                    }
                // TODO: handle the post
                }
                adapter?.notifyDataSetChanged()
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
*/

    }
}

