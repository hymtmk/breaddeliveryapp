package com.bread.breaddelivery.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bread.breaddelivery.PasswordConfirmActivity
import com.bread.breaddelivery.R
import com.bread.breaddelivery.StatisticsDetailActivity
import com.bread.breaddelivery.adapter.CustomerListAdapter
import com.bread.breaddelivery.adapter.SingleCustomerListAdapter
import com.bread.breaddelivery.model.CustomerModel
import com.bread.breaddelivery.utils.Constant
import com.google.firebase.database.*

class StatisticsFragment2 : Fragment() {
    private var listView: ListView? = null
    private var adapter: SingleCustomerListAdapter? = null
    private lateinit var customerList: ArrayList<CustomerModel>

    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_statistics2, container, false)

        // show list view
        customerList = ArrayList<CustomerModel>()
        listView = root.findViewById<ListView>(R.id.listView)
        adapter = SingleCustomerListAdapter(context!!, customerList)

        listView?.adapter = adapter
        listView?.setOnItemClickListener{ parent, view, position, id ->
            var customerModel = customerList[position]
            var intent = Intent(context, StatisticsDetailActivity::class.java)
            intent.putExtra(Constant.EXTRA_USERID, customerModel.customerId)
            intent.putExtra(Constant.EXTRA_CUSTOMERNAME, customerModel.name)
            startActivity(intent)
        }
        adapter?.notifyDataSetChanged()
        activity!!.setTitle(getString(R.string.menu_statistics))
        return root
    }

    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        mDatabaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                customerList.clear()
                // Get Post object and use the values to update the UI
                for (postSnapshot in dataSnapshot.children) {
                    var uservisible = postSnapshot.child("visible")
                    // in case of visible
//                    if(uservisible.value!!.equals(true)) {
                        customerList.add(
                            CustomerModel(
                                postSnapshot.key.toString(),
                                postSnapshot.child("customername").value.toString(),
                                postSnapshot.child("address").value.toString(),
                                postSnapshot.child("phonenumber").value.toString(),
                                postSnapshot.child("distance").value.toString(),
                                postSnapshot.child("visible").value.toString()
                            )
                        )
//                    }
                    // TODO: handle the post
                    customerList.sortBy {
                        if(it?.distance?.isEmpty())
                            it?.distance = "0"
                        it?.distance?.toDouble() }
                }
                adapter?.notifyDataSetChanged()
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
    }
}