package com.bread.breaddelivery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.core.util.rangeTo
import androidx.fragment.app.Fragment
import com.bread.breaddelivery.R
import com.bread.breaddelivery.adapter.MyOrderListAdapter
import com.bread.breaddelivery.adapter.OrderHistoryAdapter
import com.bread.breaddelivery.adapter.StorageAdapter
import com.bread.breaddelivery.model.CustomerOrder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class OrderHistoryFragment : Fragment() {

    private var listView: ListView? = null
    private var adapter: OrderHistoryAdapter? = null
    private lateinit var customerOrderList: ArrayList<CustomerOrder>

    private lateinit var auth: FirebaseAuth
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase

    private lateinit var mUserId: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_orderhistory, container, false)

        // show list view
        listView = root.findViewById<ListView>(R.id.listView)
        customerOrderList = ArrayList<CustomerOrder>()
        adapter = OrderHistoryAdapter(context!!, customerOrderList)

        listView?.adapter = adapter
        adapter?.notifyDataSetChanged()
        activity!!.setTitle(getString(R.string.menu_orderhistory))
        return root
    }

    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        var mDatabaseCustomerReference = mDatabase!!.reference!!.child("Customers")
        var customerOrderQuery = mDatabaseReference.orderByChild("date").limitToLast(50)
        customerOrderQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                customerOrderList.clear()
                // Get Post object and use the values to update the UI
                for (postSnapshot in dataSnapshot.children) {
                    var userid = postSnapshot.child("userid").value.toString()
                    val currentUserDb = mDatabaseCustomerReference?.child(userid)
                    var requestDate = postSnapshot.child("date").value.toString()
                    var smallBread = postSnapshot.child("small").value.toString()
                    var largeBread = postSnapshot.child("large").value.toString()
                    var footlongBread = postSnapshot.child("footlong").value.toString()
                    var status = postSnapshot.child("status").value.toString()
                    currentUserDb.addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            if(!checkCustomerOrderExist(postSnapshot.key.toString())) {
                                customerOrderList.add(
                                    CustomerOrder(
                                        postSnapshot.key.toString(),
                                        requestDate,
                                        smallBread,
                                        largeBread,
                                        footlongBread,
                                        status.toString(),
                                        snapshot.child("customername").value.toString(),
                                        snapshot.child("address").value.toString(),
                                        snapshot.child("phonenumber").value.toString(),
                                        snapshot.child("distance").value.toString()
                                    )
                                )
                                val sdf = SimpleDateFormat("dd/MM/yyyy")
                                customerOrderList.sortWith(compareByDescending<CustomerOrder> { it?.requestDate }
                                    .thenByDescending { sdf.parse(it?.requestDate) }
                                )
                                adapter?.notifyDataSetChanged()

                            }
                        }
                        override fun onCancelled(databaseError: DatabaseError) {}
                    })
                    // TODO: handle the post

                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
    }
    fun checkCustomerOrderExist(orderId: String):Boolean{
        for(order in customerOrderList){
            if(order.orderid.equals(orderId))
                return true
        }
        return false
    }
}

