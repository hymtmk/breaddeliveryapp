package com.bread.breaddelivery.utils

import android.content.Context
import android.content.SharedPreferences

class SessionManager(context: Context){
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"

    private val KEY_STARTDATE = "start_date"
    private val KEY_ENDDATE = "end_date"

    private val KEY_USERMODE = "user_mode"
    private val KEY_EMAIL = "email_address"
    private val KEY_USERNAME = "username"

    private val KEY_STATISTICS_PERIODMODE = "period_mode"

    private val preferences: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)

    fun setUserMode(value: Int) {
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putInt(KEY_USERMODE, value)
        editor.commit()
    }
    fun getUserMode(): Int? {
        return preferences.getInt(KEY_USERMODE, 0)
    }
    fun setPeriodMode(value: Int){
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putInt(KEY_STATISTICS_PERIODMODE, value)
        editor.commit()
    }
    fun getPeriodMode(): Int? {
        return preferences.getInt(KEY_STATISTICS_PERIODMODE, -1)
    }
    fun setStartDate(value: String){
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(KEY_STARTDATE, value)
        editor.commit()
    }
    fun getStartDate(): String?{
        return preferences.getString(KEY_STARTDATE, "")
    }
    fun setEndDate(value: String){
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(KEY_ENDDATE, value)
        editor.commit()
    }
    fun getEndDate(): String?{
        return preferences.getString(KEY_ENDDATE, "")
    }
    fun setEmail(value: String) {
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(KEY_EMAIL, value)
        editor.commit()
    }
    fun getEmail(): String? {
        return preferences.getString(KEY_EMAIL, "")
    }
    fun setUserName(value: String) {
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(KEY_USERNAME, value)
        editor.commit()
    }
    fun getUserName(): String? {
        return preferences.getString(KEY_USERNAME, "")
    }
    fun clear() {
        val editor: SharedPreferences.Editor = preferences.edit()
        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor.clear()
        editor.commit()
    }
}