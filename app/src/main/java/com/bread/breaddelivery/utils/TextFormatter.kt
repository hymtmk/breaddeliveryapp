package com.bread.breaddelivery.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*


class TextFormatter {
    companion object{
        @JvmStatic
        fun formatDate(
            formatString: String,
            dateToFormat: Date
        ): String? {
            try {
                val simpleDateFormat = SimpleDateFormat(formatString)
                return simpleDateFormat.format(dateToFormat)
            }catch (e: Exception) {
                e.printStackTrace()
            }
            return null

        }
    }
}
