package com.bread.breaddelivery.utils

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
class DateUtil {
    companion object {
        fun isThisDay(date: String): Boolean {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            try {
                var currentDate = sdf.parse(date)
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                var today = calendar.time
                if (currentDate.equals(today))
                    return true
            } catch (e: Exception) {

            }
            return false
        }
        fun isThisWeek(date: String): Boolean {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            try {
                var currentDate = sdf.parse(date)
                val targetCalendar = Calendar.getInstance()
                targetCalendar.time = currentDate
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                // this week
                var thisweek = calendar.get(Calendar.WEEK_OF_YEAR)
                var thisyear = calendar.get(Calendar.YEAR)
                var targetweek = targetCalendar.get(Calendar.WEEK_OF_YEAR)
                var targetyear = targetCalendar.get(Calendar.YEAR)
                if(thisweek == targetweek)
                    return true
            } catch (e: Exception) {

            }
            return false
        }
        fun isThisMonth(date: String): Boolean {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            try {
                var currentDate = sdf.parse(date)
                val targetCalendar = Calendar.getInstance()
                targetCalendar.time = currentDate
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                // this moth
                var thismonth = calendar.get(Calendar.MONTH)
                var targetmonth = targetCalendar.get(Calendar.MONTH)
                if(thismonth == targetmonth)
                    return true
            } catch (e: Exception) {

            }
            return false
        }
        fun isThisYear(date: String): Boolean {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            try {
                var currentDate = sdf.parse(date)
                val targetCalendar = Calendar.getInstance()
                targetCalendar.time = currentDate
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                // this moth
                var thisyear = calendar.get(Calendar.YEAR)
                var targetyear = targetCalendar.get(Calendar.YEAR)
                if(thisyear == targetyear)
                    return true
            } catch (e: Exception) {

            }
            return false
        }
        fun isBetweenStartEnd(sd: String, ed: String, cd: String): Boolean {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            try {
                var startDate = sdf.parse(sd)
                var endDate = sdf.parse(ed)
                var checkDate = sdf.parse(cd)

                if(checkDate.after(startDate) && checkDate.before(endDate))
                    return true
            } catch (e: Exception) {

            }
            return false
        }
    }
}