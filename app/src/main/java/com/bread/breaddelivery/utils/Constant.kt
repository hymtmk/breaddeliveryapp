package com.bread.breaddelivery.utils

class Constant {
    companion object {
        const val EXTRA_LOGIN_PARAM = "LOGIN_PARAM"
        const val EXTRA_CUSTOMERNAME = "CUSTOMERNAME"
        const val EXTRA_EMAIL = "EMAIL"
        const val EXTRA_USERID = "USERID"
        const val EXTRA_ORDERID = "ORDERID"
        const val EXTRA_REQUESTDATE = "REQUESTDATE"
        const val EXTRA_SMALLPCS = "SMALLPCS"
        const val EXTRA_LARGEPCS = "LARGEPCS"
        const val EXTRA_FOOTLONGPCS = "FOOTLONGPCS"
        const val EXTRA_HISTORY = "HISTORY"
    }
}