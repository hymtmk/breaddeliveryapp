package com.bread.breaddelivery

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class SignupActivity : BaseActivity() {

//    private lateinit var spinner_zone: Spinner
    private lateinit var edit_email: EditText
    private lateinit var edit_password: EditText
    private lateinit var edit_customername: EditText
    private lateinit var edit_address: EditText
    private lateinit var edit_phonenumber: EditText
    private lateinit var but_register: Button

    private lateinit var auth: FirebaseAuth
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null

//    private var zone: String? = null
    private var email: String? = null
    private var password: String? = null
    private var customername: String? = null
    private var address: String? = null
    private var phonenumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        //action bar
        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.text_register)
        actionbar.setDisplayHomeAsUpEnabled(true)
        //actionbar.setHomeButtonEnabled(true)

//        spinner_zone = findViewById<View>(R.id.spinner_zone) as Spinner
        edit_email = findViewById<View>(R.id.edit_email) as EditText
        edit_password = findViewById<View>(R.id.edit_password) as EditText
        edit_customername = findViewById<View>(R.id.edit_customername) as EditText
        edit_address = findViewById<View>(R.id.edit_address) as EditText
        edit_phonenumber = findViewById<View>(R.id.edit_phonenumber) as EditText

        but_register = findViewById<Button>(R.id.but_register)
        but_register.setOnClickListener{
            creeateNewAccount()
        }

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        auth = FirebaseAuth.getInstance()

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    private fun creeateNewAccount() {
//        zone = spinner_zone.selectedItem.toString()
        email = edit_email?.text.toString()
        password = edit_password?.text.toString()
        customername = edit_customername?.text.toString()
        address = edit_address?.text.toString()
        phonenumber = edit_phonenumber?.text.toString()
        if(!isEmailValid(email!!)){
            Toast.makeText(this, "Enter the valid email address!.", Toast.LENGTH_SHORT).show()
            return
        }
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)
            && !TextUtils.isEmpty(customername) && !TextUtils.isEmpty(address) && !TextUtils.isEmpty(phonenumber)) {
            showProgressDialog()
            auth?.createUserWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task->
                    hideProgressDialog()
                    val userId = auth!!.currentUser!!.uid

                    //update user profile information
                    val currentUserDb = mDatabaseReference!!.child(userId)
//                    currentUserDb.child("zone").setValue(zone)
                    currentUserDb.child("customername").setValue(customername)
                    currentUserDb.child("address").setValue(address)
                    currentUserDb.child("phonenumber").setValue(phonenumber)
                    currentUserDb.child("distance").setValue("")
                    currentUserDb.child("visible").setValue(true)
                    finish()
                }
        } else {
            Toast.makeText(this, getString(R.string.text_invalid_userinfo), Toast.LENGTH_SHORT).show()
        }
    }
}
