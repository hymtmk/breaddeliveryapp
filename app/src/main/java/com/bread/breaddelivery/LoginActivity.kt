package com.bread.breaddelivery

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.NetworkUtil
import com.bread.breaddelivery.utils.SessionManager
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : BaseActivity() {
    private lateinit var edit_email: EditText
    private lateinit var edit_password: EditText
    private lateinit var but_login: Button
    private lateinit var but_signup: Button
    private lateinit var but_admin: Button
    private lateinit var but_driver: Button
    private lateinit var but_all_storage: Button

    private lateinit var auth: FirebaseAuth

    private var email: String? = null
    private var password: String? = null

    private lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        edit_email = findViewById<EditText>(R.id.edit_email)
        edit_password = findViewById<EditText>(R.id.edit_password)
        but_login = findViewById<Button>(R.id.but_login)
        but_signup = findViewById<Button>(R.id.but_signup)
        but_admin = findViewById<Button>(R.id.but_admin)
        but_driver = findViewById<Button>(R.id.but_driver)
        but_all_storage = findViewById<Button>(R.id.but_all_storage)

        but_login.setOnClickListener{
            launchLogin()
        }
        but_signup.setOnClickListener{
            launchSignup()
        }
        but_admin.setOnClickListener{
            launchPasswordConfirm(0)
        }
        but_driver.setOnClickListener{
            launchPasswordConfirm(1)
        }
        but_all_storage.setOnClickListener{
            launchPasswordConfirm(2)
        }
        sessionManager = SessionManager(this)
        auth = FirebaseAuth.getInstance()
    }

    override fun onResume() {
        super.onResume()
        if(!NetworkUtil.checkNetworkConnection(this))
            popupNoConnection()

    }
    fun popupNoConnection(){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(this)
        // Set a title for alert dialog
        builder.setTitle(getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(getString(R.string.text_alert_noconnection))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { finish() }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("OK",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun launchLogin(){
        email = edit_email?.text.toString()
        password = edit_password?.text.toString()
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            showProgressDialog()
            auth?.signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    hideProgressDialog()
                    if(task.isSuccessful){
                        // save into preference
                        sessionManager.setUserMode(1)
                        launchCustomerActivity()
                    }
                    else{
                        Toast.makeText(this@LoginActivity, getString(R.string.text_auth_failed),
                            Toast.LENGTH_SHORT).show()
                    }
                }
        }
        else{
            Toast.makeText(this, getString(R.string.text_invalid_userinfo), Toast.LENGTH_SHORT).show()
        }
    }
    fun launchCustomerActivity(){
        var intent = Intent(this, CustomerActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchPasswordConfirm(mode: Int){
        var intent = Intent(this, PasswordConfirmActivity::class.java)
        intent.putExtra(Constant.EXTRA_LOGIN_PARAM, mode)
        startActivity(intent)
        finish()
    }
    fun launchSignup() {
        var intent = Intent(this, SignupActivity::class.java)
        startActivity(intent)
    }
}
