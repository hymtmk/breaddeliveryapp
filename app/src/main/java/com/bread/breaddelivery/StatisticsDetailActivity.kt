package com.bread.breaddelivery

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import android.widget.*
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.DateUtil
import com.bread.breaddelivery.utils.SessionManager
import com.bread.breaddelivery.utils.TextFormatter
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class StatisticsDetailActivity : BaseActivity() {

    private lateinit var mCustomerId: String
    private lateinit var mCustomerName: String

    private lateinit var radio_group: RadioGroup
    private lateinit var lin_range: LinearLayout
    private lateinit var lin_period: LinearLayout
    private lateinit var spinner_period: Spinner
    private var mPeriodMode: Int = -1

    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase

    private lateinit var txt_smallpcs: TextView
    private lateinit var txt_largepcs: TextView
    private lateinit var txt_footlongpcs: TextView

    private lateinit var edit_start_date: EditText
    private lateinit var edit_end_date: EditText

    private lateinit var mUserStartDate: Date
    private lateinit var mUserEndDate: Date

    private lateinit var customerOrderList: ArrayList<CustomerOrder>
    private lateinit var statisticsOrderList: ArrayList<CustomerOrder>

    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics_detail)

        sessionManager = SessionManager(this)
        val intent = getIntent()
        mCustomerId = intent.getStringExtra(Constant.EXTRA_USERID)
        mCustomerName = intent.getStringExtra(Constant.EXTRA_CUSTOMERNAME)

        val actionBar = supportActionBar
        actionBar!!.title = mCustomerName
        actionBar.setDisplayHomeAsUpEnabled(true)

        txt_smallpcs = findViewById(R.id.txt_smallpcs)
        txt_largepcs = findViewById(R.id.txt_largepcs)
        txt_footlongpcs = findViewById(R.id.txt_footlongpcs)

        edit_start_date = findViewById(R.id.edit_start_date) as EditText
        edit_start_date.setOnClickListener {
            // show date picker
            val date: Date
            date = mUserStartDate
            val calendar = Calendar.getInstance()
            calendar.time = date
            val year = calendar[Calendar.YEAR]
            val month = calendar[Calendar.MONTH]
            val day = calendar[Calendar.DAY_OF_MONTH]
            val datePickerDialog =
                DatePickerDialog(this@StatisticsDetailActivity,
                    OnDateSetListener { datePicker, i, i1, i2 -> setStartDate(i, i1, i2) },
                    year,
                    month,
                    day
                )
            datePickerDialog.show()
        }
        edit_end_date = findViewById(R.id.edit_end_date) as EditText
        edit_end_date.setOnClickListener {
            // show date picker
            val date: Date
            date = mUserEndDate
            val calendar = Calendar.getInstance()
            calendar.time = date
            val year = calendar[Calendar.YEAR]
            val month = calendar[Calendar.MONTH]
            val day = calendar[Calendar.DAY_OF_MONTH]
            val datePickerDialog =
                DatePickerDialog(this@StatisticsDetailActivity,
                    OnDateSetListener { datePicker, i, i1, i2 -> setEndDate(i, i1, i2) },
                    year,
                    month,
                    day
                )
            datePickerDialog.show()
        }
        lin_range = findViewById(R.id.lin_range)
        lin_period = findViewById(R.id.lin_period)

        radio_group = findViewById(R.id.radio_group)
        radio_group.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.radio_range->{
                    selectRange()
                }
                R.id.radio_period->{
                    selectPeriod()
                }
            }
        }
        spinner_period = findViewById(R.id.spinner_period)

        spinner_period?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                getStatisticsOrderList(position)
            }
        }
        customerOrderList = ArrayList<CustomerOrder>()
        statisticsOrderList = ArrayList<CustomerOrder>()

        // select default mode
        mPeriodMode = sessionManager.getPeriodMode()!!
        if(mPeriodMode == -1)
            radio_group.check(R.id.radio_range)
        else if(mPeriodMode>=0 && mPeriodMode <= 4){
            radio_group.check(R.id.radio_period)
            spinner_period.setSelection(mPeriodMode)
        }
        setDateAndTimeEditText()
    }
    override fun onPause() {
        super.onPause()
        var startDate = edit_start_date.text.toString()
        var endDate = edit_end_date.text.toString()
        sessionManager.setStartDate(startDate!!)
        sessionManager.setEndDate(endDate!!)
        if(radio_group.checkedRadioButtonId == R.id.radio_range)
            sessionManager.setPeriodMode(-1)
        else {
            sessionManager.setPeriodMode(spinner_period.selectedItemPosition)
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        var customerOrderQuery = mDatabaseReference.orderByChild("userid").equalTo(mCustomerId)
        customerOrderQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (postSnapshot in dataSnapshot.children) {
                    val requestDate = postSnapshot.child("date").value.toString()
                    var smallBread = postSnapshot.child("small").value.toString()
                    var largeBread = postSnapshot.child("large").value.toString()
                    var footlongBread = postSnapshot.child("footlong").value.toString()
                    var status = postSnapshot.child("status").value.toString()
                    if(status.equals("2")){
                        customerOrderList.add(CustomerOrder("", requestDate, smallBread, largeBread, footlongBread, status))
                    }
                }
                getStatisticsOrderList(mPeriodMode)
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })

    }
    fun setStartDate(year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        val time24: Boolean = DateFormat.is24HourFormat(this)
        if (time24) {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1)
        } else {
            calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + 1)
        }
        calendar.set(Calendar.MINUTE, 0)
        calendar.add(Calendar.DAY_OF_MONTH, 1)
        val hour: Int
        val minute: Int
        val reminderCalendar = Calendar.getInstance()
        reminderCalendar[year, month] = day
        if (reminderCalendar.after(calendar)) { //    Toast.makeText(this, "My time-machine is a bit rusty", Toast.LENGTH_SHORT).show();
            return
        }
        if (mUserStartDate != null) {
            calendar.time = mUserStartDate
        }
        hour = if (DateFormat.is24HourFormat(this)) {
            calendar[Calendar.HOUR_OF_DAY]
        } else {
            calendar[Calendar.HOUR]
        }
        minute = calendar[Calendar.MINUTE]
        calendar[year, month, day, hour] = minute
        mUserStartDate = calendar.time
        val dateFormat = "dd/MM/yyyy"
        var startDate = TextFormatter.formatDate(dateFormat, mUserStartDate)
        edit_start_date.setText(startDate)

        getStatisticsOrderList(-1)
    }
    fun setEndDate(year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        val hour: Int
        val minute: Int
        val reminderCalendar = Calendar.getInstance()
        reminderCalendar[year, month] = day
        if (reminderCalendar.after(calendar)) {
            return
        }
        if (mUserEndDate != null) {
            calendar.time = mUserEndDate
        }
        hour = if (DateFormat.is24HourFormat(this)) {
            calendar[Calendar.HOUR_OF_DAY]
        } else {
            calendar[Calendar.HOUR]
        }
        minute = calendar[Calendar.MINUTE]
        calendar[year, month, day, hour] = minute
        mUserEndDate = calendar.time
        val dateFormat = "dd/MM/yyyy"
        var endDate = TextFormatter.formatDate(dateFormat, mUserEndDate)
        edit_end_date.setText(endDate)

        getStatisticsOrderList(-1)
    }
    private fun setDateAndTimeEditText() {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        var startDate = sessionManager.getStartDate()
        var endDate = sessionManager.getEndDate()
        if(startDate!!.isEmpty() || endDate!!.isEmpty()) {
            val cal: Calendar = Calendar.getInstance()
            val time24: Boolean = DateFormat.is24HourFormat(this)
            if (time24) {
                cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 1)
            } else {
                cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 1)
            }
            cal.set(Calendar.MINUTE, 0)
            mUserStartDate = cal.time
            startDate = TextFormatter.formatDate("dd/MM/yyyy", mUserStartDate)!!
            mUserEndDate = cal.time
            endDate = TextFormatter.formatDate("dd/MM/yyyy", mUserEndDate)!!
        }
        else{
            mUserStartDate = sdf.parse(startDate)
            mUserEndDate = sdf.parse(endDate)

        }
        edit_start_date.setText(startDate)
        edit_end_date.setText(endDate)
    }
    fun selectRange(){
        disableLayout(R.id.lin_range, true)
        disableLayout(R.id.lin_period, false)

        getStatisticsOrderList(-1)
    }
    fun selectPeriod() {
        disableLayout(R.id.lin_range, false)
        disableLayout(R.id.lin_period, true)

        getStatisticsOrderList(spinner_period.selectedItemPosition)
    }
    fun disableLayout(id: Int, flag: Boolean){
        var layout = findViewById<LinearLayout>(id)
        for (i in 0 until layout.getChildCount()) {
            val child: View = layout.getChildAt(i)
            child.setEnabled(flag)
        }
    }

    fun getStatisticsOrderList(pos: Int){
        var smallPcs :Int = 0
        var largePcs :Int = 0
        var footlongPcs :Int = 0
        var startDate = edit_start_date.text.toString()
        var endDate = edit_end_date.text.toString()

        for (childSnapshot in customerOrderList) {
            var requestDate = childSnapshot.requestDate
            if(pos == -1){
                if(!DateUtil.isBetweenStartEnd(startDate, endDate,requestDate))
                    continue
            }
            else if(pos == 0) {
                if (!DateUtil.isThisDay(requestDate))
                    continue
            }
            else if(pos == 1){
                if(!DateUtil.isThisWeek(requestDate))
                    continue
            }
            else if(pos == 2){
                if(!DateUtil.isThisMonth(requestDate))
                    continue
            }
            else if(pos == 3) {
                if (!DateUtil.isThisYear(requestDate))
                    continue
            }
            var smallVal = childSnapshot.small
            var largeVal = childSnapshot.large
            var footlongVal = childSnapshot.footlong
            if(!smallVal.isEmpty())
                smallPcs = smallPcs +  smallVal.toInt()
            if(!largeVal.isEmpty())
                largePcs = largePcs +  largeVal.toInt()
            if(!footlongVal.isEmpty())
                footlongPcs = footlongPcs +  footlongVal.toInt()
            txt_smallpcs.setText(smallPcs.toString())
            txt_largepcs.setText(largePcs.toString())
            txt_footlongpcs.setText(footlongPcs.toString())
        }
    }
}
