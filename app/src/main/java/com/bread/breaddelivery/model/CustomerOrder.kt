package com.bread.breaddelivery.model

class CustomerOrder{
    var orderid: String = ""
//    var zone: String = ""
    var customer: String = ""
    var address: String=""
    var phonenumber: String=""
    var distance: String=""
    // types of bread
    var requestDate: String = ""
    var small: String = ""
    var large: String = ""
    var footlong: String = ""
    var status: String = ""
    var userId: String = ""
    constructor(){}
    constructor(orderid: String, requestDate: String, small: String, large: String, footlong: String, status: String, customer: String = "", address:String = "", phonenumber: String = "", distance: String = "", userid: String = ""){
        this.orderid = orderid
        this.requestDate = requestDate
        this.small = small
        this.large = large
        this.footlong = footlong
        this.status = status
//        this.zone = zone
        this.customer = customer
        this.address = address
        this.phonenumber = phonenumber
        this.distance = distance
        this.userId = userid
    }
}