package com.bread.breaddelivery.model

class CustomerModel{
//    var zoneId: Int = 0
    var customerId: String = ""
    var name: String = ""
    var address: String=""
    var phoneNumber: String = ""
    var distance: String = ""
    var visible: String = ""
    constructor(){}
    constructor(customerId:String, name: String, address:String, phoneNumber: String, distance: String, visible: String){
//        this.zoneId = zoneId
        this.customerId = customerId
        this.name = name
        this.address = address
        this.phoneNumber = phoneNumber
        this.distance = distance
        this.visible = visible
    }
}