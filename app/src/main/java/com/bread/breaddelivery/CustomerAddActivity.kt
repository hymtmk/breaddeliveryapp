package com.bread.breaddelivery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class CustomerAddActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_add)

        //action bar
        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.text_add_customer)
        actionbar.setDisplayHomeAsUpEnabled(true)
        //actionbar.setHomeButtonEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
