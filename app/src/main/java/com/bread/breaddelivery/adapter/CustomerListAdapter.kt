package com.bread.breaddelivery.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bread.breaddelivery.EditCustomerActivity
import com.bread.breaddelivery.EditProfileActivity
import com.bread.breaddelivery.R
import com.bread.breaddelivery.model.CustomerModel
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.Constant
import com.google.firebase.database.FirebaseDatabase

class CustomerListAdapter(private val context: Context,
                          private val dataSource: ArrayList<CustomerModel>) : BaseAdapter() {

    private class ViewHolder(row: View?) {
//        var txt_zone: TextView? = null
        var txt_name: TextView? = null
        var txt_address: TextView? = null
        var txt_phonenumber: TextView? = null
        var txt_distance: TextView? = null
        var but_edit: Button? = null
        var but_delete: Button? = null

        init {
//            this.txt_zone = row?.findViewById<TextView>(R.id.txt_zone)
            this.txt_name = row?.findViewById<TextView>(R.id.txt_name)
            this.txt_address = row?.findViewById<TextView>(R.id.txt_address)
            this.txt_phonenumber = row?.findViewById<TextView>(R.id.txt_phonenumber)
            this.txt_distance = row?.findViewById<TextView>(R.id.txt_distance)
            this.but_edit = row?.findViewById<Button>(R.id.but_edit)
            this.but_delete = row?.findViewById<Button>(R.id.but_delete)
        }
    }
    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_customer, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        /*
        if(position % 2 == 0){
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.lightBlue))
        }
        else{
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.white_color))
        }
         */
        var userDto = dataSource[position]
//        viewHolder.txt_zone?.text = "Zone" + userDto.zoneId.toString()
        viewHolder.txt_name?.text = userDto.name
        viewHolder.txt_address?.text = userDto.address
        viewHolder.txt_phonenumber?.text = userDto.phoneNumber
        if(userDto.distance.isEmpty())
            userDto.distance = "0"
        viewHolder.txt_distance?.text = userDto.distance + "km"
        viewHolder.but_edit?.setOnClickListener{ view->
            launchEditProfileActivity(userDto)
        }
        viewHolder.but_delete?.setOnClickListener{ view->
            deleteCustomer(userDto)
        }
        return view as View
    }
    fun deleteCustomer(customer: CustomerModel){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(context)
        // Set a title for alert dialog
        builder.setTitle(context.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(context.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { processConfirm(customer) }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun processConfirm(customer: CustomerModel){
        var mDatabase = FirebaseDatabase.getInstance()
        var mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        //delete order node
        //delete order node
        var currentCustomerDb = mDatabaseReference!!.child(customer.customerId!!)
        currentCustomerDb.child("visible").setValue(false)
//        dataSource.remove(myOrder)
//        notifyDataSetChanged()
    }
    fun launchEditProfileActivity(customer: CustomerModel){
        var intent = Intent(context, EditCustomerActivity::class.java)
        intent.putExtra(Constant.EXTRA_USERID, customer.customerId)
        context.startActivity(intent)
    }
}
