package com.bread.breaddelivery.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bread.breaddelivery.AddOrderActivity
import com.bread.breaddelivery.EditOrderActivity
import com.bread.breaddelivery.R
import com.bread.breaddelivery.listener.OrderEditDeleteListener
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.Constant
import com.google.firebase.database.FirebaseDatabase

class MyOrderListAdapter(private val context: Context,
                         private val dataSource: ArrayList<CustomerOrder>
) : BaseAdapter() {

        private class ViewHolder(row: View?) {
        var txt_requestdate: TextView? = null
        var txt_smallpcs: TextView? = null
        var txt_largepcs: TextView? = null
        var txt_footlongpcs: TextView? = null
        var but_edit: Button? = null
        var but_delete: Button? = null

        init {
            this.txt_requestdate = row?.findViewById<TextView>(R.id.txt_requestdate)
            this.txt_smallpcs = row?.findViewById<TextView>(R.id.txt_smallpcs)
            this.txt_largepcs = row?.findViewById<TextView>(R.id.txt_largepcs)
            this.txt_footlongpcs = row?.findViewById<TextView>(R.id.txt_footlongpcs)
            this.but_edit = row?.findViewById<Button>(R.id.but_edit)
            this.but_delete = row?.findViewById<Button>(R.id.but_delete)
        }

    }

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_myorder, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        var userDto = dataSource[position]
        viewHolder.txt_requestdate?.text = userDto.requestDate

        if(userDto.small.isEmpty())
            userDto.small = "0"
        if(userDto.large.isEmpty())
            userDto.large = "0"
        if(userDto.footlong.isEmpty())
            userDto.footlong = "0"

        viewHolder.txt_smallpcs?.text = userDto.small
        viewHolder.txt_largepcs?.text = userDto.large
        viewHolder.txt_footlongpcs?.text = userDto.footlong

        viewHolder.but_edit?.setOnClickListener{ view ->
            if(userDto.status.toInt() == 0){
                launchOrderEditActivity(userDto);
            }
        }
        viewHolder.but_delete?.setOnClickListener{ view ->
            if(userDto.status.toInt() == 0){
                deleteOrder(userDto)
            }
        }

        if(userDto.status != null && userDto.status.equals("1")){
            // accpeted
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.light_yellow))
        }
        else if(userDto.status != null && userDto.status.equals("2")){
            // finished
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.light_green))
        }
        else if(userDto.status != null && userDto.status.equals("3")){
            // finished
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.red_light_color))
        }
        else{
            // default
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.white_color))
        }

        return view as View
    }
    fun launchOrderEditActivity(myOrder: CustomerOrder){
        var intent = Intent(context, EditOrderActivity::class.java)
        intent.putExtra(Constant.EXTRA_ORDERID, myOrder.orderid)
        intent.putExtra(Constant.EXTRA_REQUESTDATE, myOrder.requestDate)
        intent.putExtra(Constant.EXTRA_SMALLPCS, myOrder.small)
        intent.putExtra(Constant.EXTRA_LARGEPCS, myOrder.large)
        intent.putExtra(Constant.EXTRA_FOOTLONGPCS, myOrder.footlong)
        context.startActivity(intent)
    }
    fun deleteOrder(myOrder: CustomerOrder){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(context)
        // Set a title for alert dialog
        builder.setTitle(context.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(context.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { processConfirm(myOrder) }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun processConfirm(myOrder: CustomerOrder){
        var mDatabase = FirebaseDatabase.getInstance()
        var mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        //delete order node
        mDatabaseReference!!.child(myOrder.orderid).removeValue()
//        dataSource.remove(myOrder)
//        notifyDataSetChanged()
    }

}
