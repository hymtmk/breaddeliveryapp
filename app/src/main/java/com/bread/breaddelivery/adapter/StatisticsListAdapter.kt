package com.bread.breaddelivery.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import com.bread.breaddelivery.R
import com.bread.breaddelivery.model.CustomerOrder
import com.google.firebase.database.FirebaseDatabase


class StatisticsListAdapter(private val context: Context,
                         private val dataSource: ArrayList<CustomerOrder>) : BaseAdapter() {

    private class ViewHolder(row: View?) {
        var txt_customer: TextView? = null
        var txt_address: TextView? = null
        var txt_phonenumber: TextView? = null
        var txt_distance: TextView? = null
        var txt_smallpcs: TextView? = null
        var txt_largepcs: TextView? = null
        var txt_footlongpcs: TextView? = null

        init {
            this.txt_customer = row?.findViewById<TextView>(R.id.txt_customer)
            this.txt_address = row?.findViewById<TextView>(R.id.txt_address)
            this.txt_phonenumber = row?.findViewById<TextView>(R.id.txt_phonenumber)
            this.txt_distance = row?.findViewById<TextView>(R.id.txt_distance)
            this.txt_smallpcs = row?.findViewById<TextView>(R.id.txt_smallpcs)
            this.txt_largepcs = row?.findViewById<TextView>(R.id.txt_largepcs)
            this.txt_footlongpcs = row?.findViewById<TextView>(R.id.txt_footlongpcs)
        }
    }
    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_statistics, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        /*
        if(position % 2 == 0){
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.lightBlue))
        }
        else{
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.white_color))
        }
         */
        var userDto = dataSource[position]
        viewHolder.txt_customer?.text = userDto.customer
        viewHolder.txt_address?.text = userDto.address
        viewHolder.txt_phonenumber?.text = userDto.phonenumber
        if(userDto.distance.isEmpty())
            userDto.distance = "0"
        viewHolder.txt_distance?.text = userDto.distance + "km"

        if(userDto.small.isEmpty())
            userDto.small = "0"
        if(userDto.large.isEmpty())
            userDto.large = "0"
        if(userDto.footlong.isEmpty())
            userDto.footlong = "0"

        viewHolder.txt_smallpcs?.text = userDto.small
        viewHolder.txt_largepcs?.text = userDto.large
        viewHolder.txt_footlongpcs?.text = userDto.footlong
        return view as View
    }
}