package com.bread.breaddelivery.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bread.breaddelivery.EditCustomerActivity
import com.bread.breaddelivery.EditProfileActivity
import com.bread.breaddelivery.R
import com.bread.breaddelivery.model.CustomerModel
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.Constant
import com.google.firebase.database.FirebaseDatabase

class SingleCustomerListAdapter(private val context: Context,
                                private val dataSource: ArrayList<CustomerModel>) : BaseAdapter() {

    private class ViewHolder(row: View?) {
        var txt_name: TextView? = null
        init {
            this.txt_name = row?.findViewById<TextView>(R.id.txt_name)
        }
    }
    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_singlecustomer, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        /*
        if(position % 2 == 0){
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.lightBlue))
        }
        else{
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.white_color))
        }
         */
        var userDto = dataSource[position]
//        viewHolder.txt_zone?.text = "Zone" + userDto.zoneId.toString()
        viewHolder.txt_name?.text = userDto.name
        return view as View
    }
}
