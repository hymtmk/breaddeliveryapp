package com.bread.breaddelivery.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bread.breaddelivery.R
import com.bread.breaddelivery.model.CustomerOrder
import com.google.firebase.database.FirebaseDatabase
import fcm.androidtoandroid.FirebasePush
import fcm.androidtoandroid.model.Notification
import org.json.JSONObject

class StorageAdapter(private val context: Context,
                     private val dataSource: ArrayList<CustomerOrder>) : BaseAdapter() {

    private class ViewHolder(row: View?) {
        var txt_requestdate: TextView? = null
        var txt_customer: TextView? = null
        var txt_address: TextView? = null
        var txt_smallpcs: TextView? = null
        var txt_largepcs: TextView? = null
        var txt_footlongpcs: TextView? = null
        var but_accept: Button? = null
        var but_decline: Button? = null

        init {
            this.txt_requestdate = row?.findViewById<TextView>(R.id.txt_requestdate)
            this.txt_customer = row?.findViewById<TextView>(R.id.txt_customer)
            this.txt_address = row?.findViewById<TextView>(R.id.txt_address)
            this.txt_smallpcs = row?.findViewById<TextView>(R.id.txt_smallpcs)
            this.txt_largepcs = row?.findViewById<TextView>(R.id.txt_largepcs)
            this.txt_footlongpcs = row?.findViewById<TextView>(R.id.txt_footlongpcs)
            this.but_accept = row?.findViewById<Button>(R.id.but_accept)
            this.but_decline = row?.findViewById<Button>(R.id.but_decline)

        }
    }
    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_storage, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        /*
        if(position % 2 == 0){
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.lightBlue))
        }
        else{
            view?.setBackgroundColor(ContextCompat.getColor(context, R.color.white_color))
        }
         */
        var userDto = dataSource[position]
        viewHolder.txt_requestdate?.text = userDto.requestDate
        viewHolder.txt_customer?.text = userDto.customer
        viewHolder.txt_address?.text = userDto.address

        if(userDto.small.isEmpty())
            userDto.small = "0"
        if(userDto.large.isEmpty())
            userDto.large = "0"
        if(userDto.footlong.isEmpty())
            userDto.footlong = "0"

        viewHolder.txt_smallpcs?.text = userDto.small
        viewHolder.txt_largepcs?.text = userDto.large
        viewHolder.txt_footlongpcs?.text = userDto.footlong
        viewHolder.but_accept?.setOnClickListener{ view->
            if(userDto.status.toInt() == 0){
                aceptOrder(userDto)
            }
        }
        viewHolder.but_decline?.setOnClickListener{ view ->
            if(userDto.status.toInt() == 0){
                deleteOrder(userDto)
            }
        }
        return view as View
    }
    fun aceptOrder(myOrder: CustomerOrder){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(context)
        // Set a title for alert dialog
        builder.setTitle(context.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(context.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { processConfirmOrder(myOrder) }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun processConfirmOrder(myOrder: CustomerOrder) {
        var mDatabase = FirebaseDatabase.getInstance()
        var mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        //delete order node
        var currentOrderDb = mDatabaseReference!!.child(myOrder.orderid!!)
        currentOrderDb.child("status").setValue(1)
        sendDriver(myOrder.customer)
        sendCustomerOrderAccept(myOrder.userId)
        sendAdminOrderAccept(myOrder.customer)
    }
    fun deleteOrder(myOrder: CustomerOrder){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(context)
        // Set a title for alert dialog
        builder.setTitle(context.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(context.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { processConfirmDelete(myOrder) }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun processConfirmDelete(myOrder: CustomerOrder){
        var mDatabase = FirebaseDatabase.getInstance()
        var mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        //delete order node
        var currentOrderDb = mDatabaseReference!!.child(myOrder.orderid!!)
        currentOrderDb.child("status").setValue(3)
        sendCustomerOrderDecline(myOrder.userId)
        sendAdminOrderDecline(myOrder.customer)
    }
    private fun sendDriver(customerName: String){
        val jsonData= JSONObject()
        jsonData.put("type","driver_order")
        jsonData.put("text","Nova porudžbina za " + customerName + " je stigla, klikni da pogledaš!")
        FirebasePush.build(context.getString(R.string.fcm_legacy_server_key))
            .setData(jsonData)
            .sendToTopic(context.getString(R.string.topic_driver))
    }
    private fun sendCustomerOrderAccept(userId: String){
        val jsonData= JSONObject()
        jsonData.put("type","accept_order")
        jsonData.put("text","Porudžbina je prihvaćena i biće Vam dostavljena!")
        FirebasePush.build(context.getString(R.string.fcm_legacy_server_key))
            .setData(jsonData)
            .sendToTopic(userId)
    }
    private fun sendCustomerOrderDecline(userId: String){
        val jsonData= JSONObject()
        jsonData.put("type","decline_order")
        jsonData.put("text","Porudžbina je odbijena!")
        FirebasePush.build(context.getString(R.string.fcm_legacy_server_key))
            .setData(jsonData)
            .sendToTopic(userId)
    }

    private fun sendAdminOrderAccept(customerName: String){
        val jsonData= JSONObject()
        jsonData.put("type","admin_accept_order")
        jsonData.put("text","Porudžbina za " + customerName + " je prihvaćena!")

        FirebasePush.build(context.getString(R.string.fcm_legacy_server_key))
            .setData(jsonData)
            .sendToTopic(context.getString(R.string.topic_admin))
    }
    private fun sendAdminOrderDecline(customerName: String){
        val jsonData= JSONObject()
        jsonData.put("type","admin_decline_order")
        jsonData.put("text","Porudžbina za " + customerName + " je odbijena!")
        FirebasePush.build(context.getString(R.string.fcm_legacy_server_key))
            .setData(jsonData)
            .sendToTopic(context.getString(R.string.topic_admin))
    }

}
