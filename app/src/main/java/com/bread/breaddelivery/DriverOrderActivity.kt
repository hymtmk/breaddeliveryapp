package com.bread.breaddelivery

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bread.breaddelivery.adapter.DriverOrderAdapter
import com.bread.breaddelivery.model.CustomerOrder
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.SessionManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DriverOrderActivity : AppCompatActivity() {

    private var mUserMode: Int = 1
    private lateinit var mUserName: String
    private var listView: ListView? = null
    private var adapter: DriverOrderAdapter? = null

    private lateinit var customerOrderList: ArrayList<CustomerOrder>
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase

    private lateinit var sessionManager: SessionManager
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driverorder)

        sessionManager = SessionManager(this)
        when (mUserMode) {
            1 -> mUserName = getString(R.string.text_driver_zone1)
        }
        val actionBar = supportActionBar
        actionBar!!.title = mUserName

        customerOrderList = ArrayList<CustomerOrder>()
        // show list view
        listView = findViewById<ListView>(R.id.listView)
        adapter = DriverOrderAdapter(this, customerOrderList)

        listView?.adapter = adapter
        adapter?.notifyDataSetChanged()

        auth = FirebaseAuth.getInstance()

        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.topic_driver))

    }
    override fun onStart() {
        super.onStart()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("CustomerOrders")
        var mDatabaseCustomerReference = mDatabase!!.reference!!.child("Customers")

        mDatabaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                customerOrderList.clear()

                // Get Post object and use the values to update the UI
                for (postSnapshot in dataSnapshot.children) {
                    var userid = postSnapshot.child("userid").value.toString()
                    val currentUserDb = mDatabaseCustomerReference!!.child(userid)
                    var requestDate = postSnapshot.child("date").value.toString()
                    var smallBread = postSnapshot.child("small").value.toString()
                    var largeBread = postSnapshot.child("large").value.toString()
                    var footlongBread = postSnapshot.child("footlong").value.toString()
                    var status = postSnapshot.child("status").value.toString()
                    currentUserDb.addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            // in case of customer order is accepted and the date
                            if(status.equals("1") && !checkCustomerOrderExist(postSnapshot.key.toString()) && isThisDay(postSnapshot.child("date").value.toString())) {
                                customerOrderList.add(
                                    CustomerOrder(
                                        postSnapshot.key.toString(),
                                        requestDate,
                                        smallBread,
                                        largeBread,
                                        footlongBread,
                                        status.toString(),
                                        snapshot.child("customername").value.toString(),
                                        snapshot.child("address").value.toString(),
                                        snapshot.child("phonenumber").value.toString(),
                                        snapshot.child("distance").value.toString(),
                                        userid
                                    )
                                )
                            }
                            customerOrderList.sortedBy {
                                if(it?.distance?.isEmpty())
                                    it?.distance = "0"
                                it?.distance?.toDouble()
                            }
                            adapter?.notifyDataSetChanged()
                        }
                        override fun onCancelled(databaseError: DatabaseError) {}
                    })
                    // TODO: handle the post
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Item failed, log a message
            }
        })
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_logout -> {
                promptAlert()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
    override fun onBackPressed() {
        //super.onBackPressed()
    }
    fun promptAlert(){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(this)
        // Set a title for alert dialog
        builder.setTitle(this.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(this.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { logoutActivity() }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun logoutActivity(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(getString(R.string.topic_driver))
        auth?.signOut()
        sessionManager?.clear()
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun isThisDay(date: String):Boolean{
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        try {
            var currentDate = sdf.parse(date)
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            var today = calendar.time
            if(currentDate.equals(today))
                return true
        }
        catch (e: Exception){

        }
        return false
    }
    fun checkCustomerOrderExist(orderId: String):Boolean{
        for(order in customerOrderList){
            if(order.orderid.equals(orderId))
                return true
        }
        return false
    }
}
