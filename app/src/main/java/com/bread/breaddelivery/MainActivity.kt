package com.bread.breaddelivery

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.bread.breaddelivery.fragment.CustomerListFragment
import com.bread.breaddelivery.fragment.OrderHistoryFragment
import com.bread.breaddelivery.fragment.StatisticsFragment
import com.bread.breaddelivery.fragment.StatisticsFragment2
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.SessionManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.RemoteMessage
import fcm.androidtoandroid.FirebasePush
import fcm.androidtoandroid.model.Notification

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var toolbar: Toolbar
    private lateinit var drawer_layout: DrawerLayout
    private lateinit var navView: NavigationView

    private lateinit var sessionManager: SessionManager
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        sessionManager = SessionManager(this)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.hide()
        drawer_layout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, 0, 0
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        // select first menu
        var indexPage: Int = 0
        val isHistory = intent.getIntExtra(Constant.EXTRA_HISTORY, 0)
        if(isHistory != 0)
            indexPage = 2
        navView.getMenu().getItem(indexPage).setChecked(true);
        onNavigationItemSelected(navView.getMenu().getItem(0));

        auth = FirebaseAuth.getInstance()


        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.topic_admin))
    }
    fun onFinishPush(){

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_logout -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        displaySelectedScreen(menuItem.itemId)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            //super.onBackPressed()
        }
    }
    private fun displaySelectedScreen(itemId: Int) {

        //creating fragment object
        var fragment: Fragment? = null

        //initializing the fragment object which is selected
        when (itemId) {
            R.id.nav_customers -> fragment =
                CustomerListFragment()
            R.id.nav_statistics -> fragment =
                StatisticsFragment2()
            R.id.nav_orderhistory -> fragment =
                OrderHistoryFragment()
            R.id.nav_logout -> {
                promptAlert()
            }
        }
        //replacing the fragment
        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, fragment)
            ft.commit()
        }
    }
    fun promptAlert(){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(this)
        // Set a title for alert dialog
        builder.setTitle(this.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(this.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { logoutActivity() }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun logoutActivity(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(getString(R.string.topic_admin))
        auth?.signOut()
        sessionManager?.clear()
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchCustomerAddActivity(){
        var intent = Intent(this, CustomerAddActivity::class.java)
        startActivity(intent)
    }
}
