package com.bread.breaddelivery

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.bread.breaddelivery.adapter.DriverOrderAdapter
import com.bread.breaddelivery.fragment.CustomerListFragment
import com.bread.breaddelivery.fragment.MyOrderListFragment
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.SessionManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging


class CustomerActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var toolbar: Toolbar
    private lateinit var drawer_layout: DrawerLayout
    private lateinit var navView: NavigationView

    private lateinit var txt_username: TextView
    private lateinit var txt_email: TextView

    private lateinit var sessionManager: SessionManager
    private lateinit var auth: FirebaseAuth
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null

    private lateinit var mUserId: String
    private lateinit var mEmail:String
    private lateinit var mUserName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle(R.string.app_name)
        // session manager
        sessionManager = SessionManager(this)
        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            launchAddOrderActivity()
        }
        drawer_layout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, 0, 0
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        // select first menu
        navView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navView.getMenu().getItem(0));

        // Get Navigation Header
        val headerView: View = navView.getHeaderView(0)
        txt_username = headerView?.findViewById(R.id.txt_username)
        txt_email = headerView?.findViewById(R.id.txt_email)

    }
    override fun onStart() {
        super.onStart()
        auth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Customers")
        mUserId = auth!!.currentUser!!.uid
        //update user profile information
        val currentUserDb = mDatabaseReference!!.child(mUserId)

        showProgressDialog()
        currentUserDb.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                mUserName = snapshot.child("customername").value as String
                mEmail = auth.currentUser!!.email!!
                // setText
                txt_username.setText(mUserName)
                txt_email.setText(mEmail)
                hideProgressDialog()
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
        FirebaseMessaging.getInstance().subscribeToTopic(mUserId)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        // menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        displaySelectedScreen(menuItem.itemId)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
//            super.onBackPressed()
        }
    }
    public fun setTitle(title: String){
        supportActionBar!!.setTitle(title)
    }
    private fun displaySelectedScreen(itemId: Int) {

        //creating fragment object
        var fragment: Fragment? = null

        //initializing the fragment object which is selected
        when (itemId) {
            R.id.nav_myorders -> fragment =
                MyOrderListFragment()
            R.id.nav_editprofile ->
                launchEditProfileActivity()
            R.id.nav_logout -> {
                promptAlert()
            }
        }
        //replacing the fragment
        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, fragment)
            ft.commit()
        }
    }
    fun promptAlert(){
        lateinit var dialog: AlertDialog
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(this)
        // Set a title for alert dialog
        builder.setTitle(this.getString(R.string.app_name))
        // Set a message for alert dialog
        builder.setMessage(this.getString(R.string.text_alert_confirm))
        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE -> { logoutActivity() }
                DialogInterface.BUTTON_NEGATIVE -> {}
            }
        }
        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES",dialogClickListener)
        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO",dialogClickListener)
        // Initialize the AlertDialog using builder object
        dialog = builder.create()
        // Finally, display the alert dialog
        dialog.show()
    }
    fun logoutActivity(){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(mUserId)
        auth?.signOut()
        sessionManager?.clear()
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchAddOrderActivity(){
        var intent = Intent(this, AddOrderActivity::class.java)
        intent.putExtra(Constant.EXTRA_USERID, mUserId)
        intent.putExtra(Constant.EXTRA_EMAIL, mEmail)
        intent.putExtra(Constant.EXTRA_CUSTOMERNAME, mUserName)
        startActivity(intent)
    }
    fun launchEditProfileActivity(){
        var intent = Intent(this, EditProfileActivity::class.java)
        intent.putExtra(Constant.EXTRA_USERID, mUserId)
        startActivity(intent)
    }
}
