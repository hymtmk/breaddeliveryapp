package com.bread.breaddelivery

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.bread.breaddelivery.utils.Constant
import com.bread.breaddelivery.utils.SessionManager

class SplashActivity : AppCompatActivity() {
    var mUserMode:Int = 0
    lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sessionManager = SessionManager(this)
        Handler().postDelayed({
            mUserMode = sessionManager.getUserMode()!!
            when(mUserMode){
                1->{
                    launchCustomerActivity()
                }
                2->{
                    launchMainActivity()
                }
                3->{
                    launchDriverActivity()
                }
                4->{
                    launchStorageActivity()
                }
                else->{
                    launchLoginActivity()
                }
            }
        }, 2000)
    }
    fun launchLoginActivity(){
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchCustomerActivity(){
        var intent = Intent(this, CustomerActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchMainActivity(){
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchDriverActivity(){
        var intent = Intent(this, DriverOrderActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun launchStorageActivity(){
        var intent = Intent(this, StorageActivity::class.java)
        startActivity(intent)
        finish()
    }

}
